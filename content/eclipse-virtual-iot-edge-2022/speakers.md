---
title: "Speakers"
seo_title: "Virtual IoT & Edge Days 2022 | Eclipse IoT"
date: 2022-04-14
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
layout: "speakers"
---

{{< events/user_bios event="eclipse-virtual-iot-edge-2022" source="speakers" imgRoot="/assets/images/speakers/" >}}
