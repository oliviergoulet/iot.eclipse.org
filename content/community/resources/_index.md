---
title: "Resources"
seo_title: "Resources - Eclipse IoT Technologies"
description: "Explore a variety of Eclipse IoT technologies through surveys, articles, case studies, whitepapers, and market reports."
keywords: ["Eclipse IoT", "Eclipse IoT technologies"]
aliases:
    - /resources/
layout: "single"
---

#### Below are some useful links to articles and other resources about Eclipse IoT Technologies.

{{< grid/div class="resources padding-bottom-30" isMarkdown="false" >}}
<h2>Videos</h2>
{{< youtube  "videoseries?list=PLy7t4z5SYNaRorMpk1txiehpkyeI0JLn-">}}
{{</ grid/div >}}

{{< newsroom/resources wg="eclipse_iot" title="Surveys, Case Studies, White Papers, Market Reports" type="survey_report, case_study, white_paper, market_report" template="image-with-title" limit="3" class="col-md-8" viewMorePath="iot-surveys, case-studies, white-papers, market-reports" >}}


{{< grid/div class="resources" isMarkdown="false" >}}
{{< newsroom/news
    title="News"
    id="news-list-container"
    publishTarget="eclipse_iot"
    count="1"
    class="res-news"
    templateId="custom-news-template" 
    templatePath="/js/templates/news-home.mustache" 
>}}
<p class="text-right resources-more"> 
    <a class="margin-right-10" href="https://newsroom.eclipse.org/node/add/news">Submit News</a>
    <a href="/news">View More</a>
</p>
{{</ grid/div >}}