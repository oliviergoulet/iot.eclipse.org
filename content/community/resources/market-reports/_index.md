---
title: "Market Reports"
description: "iot.eclipse.org is all about getting the M2M developers involved in what is happening in the different Eclipse projects"
icon: "bar-chart-2"
weight: 5
layout: "single"
---

{{< newsroom/resources wg="eclipse_iot" type="market_report" template="image-with-title" class="col-md-8" >}}
